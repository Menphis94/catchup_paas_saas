﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class Factures
    {
        public int FacturesId { get; set; }
        public int? UsersId { get; set; }

        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set; }
        public int? OrderId { get; set; }

        [ForeignKey("OrderId")]
        public virtual Order MyOrder { get; set; }
        public DateTime DateAchat { get; set; }
        public double Prix { get; set; }
        public double Tva { get; set; }
        public double Ht { get; set; }
        public double Total { get; set; }

    }
}
