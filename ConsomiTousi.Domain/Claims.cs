﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class Claims
    {

        public int ClaimsId { get; set; }
        public DateTime ClaimsDate { get; set; }
        [DataType(DataType.MultilineText)]
        public string Description { get; set; }
        public string ClaimsState { get; set; }

        public int? ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product MyProduct { get; set; }

        public int? UsersId { get; set; }

        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set; }
    }
}
