﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class Shop
    {
        public int ShopId { get; set; }
        public string NameShop { get; set; }
        [DataType(DataType.MultilineText)]
        public string DescriptionShop { get; set; }
        public DateTime DateShop { get; set; }
    }
}
