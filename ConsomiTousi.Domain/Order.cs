﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public class Order
    {

        public int OrderId { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderState { get; set; }

        public int? ProductId { get; set; }

        [ForeignKey("ProductId")]
        public virtual Product MyProduct { get; set; }

        public int? UsersId { get; set; }

        [ForeignKey("UsersId")]
        public virtual Users MyUsers { get; set; }

    }
}
