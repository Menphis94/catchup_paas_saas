﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTousi.Domain
{
   public  class Users
    {
        public int UsersId { get; set; }
        public string UsersName { get; set; }
        public string UsersLastname { get; set; }
        public string UsersEmail { get; set; }
        public string UsersCin { get; set; }
        public string UsersAdress { get; set; }
        public string UsersPhone { get; set; }
        public string UsersPays { get; set; }
        public string UsersRegion { get; set; }
        public string UsersRole { get; set; }
        public DateTime UsersDate { get; set; }
        public DateTime DateNaissance { get; set; }
        public string UsersState { get; set; }


    }
}
