﻿using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsimiTounsi.Data
{
    public class ConsomiCtxt : DbContext
    {


        public DbSet<Product> Products { get; set; }
        
        public DbSet<Category> Categories { get; set; }
        public DbSet<Claims> Claimes { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Users> User { get; set; }
        public DbSet<Factures> Facturs { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public ConsomiCtxt(): base("name=MaChaine")
        {

        }
    }
}
