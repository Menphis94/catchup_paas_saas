﻿using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{

   
    public class ClaimsController : Controller
    {

        IDataBaseFactory dbf;
        IUnitOfWork uow;
        IService<Claims> claimS;
        IService<Product> produictS;

        public ClaimsController()
        {
            dbf = new DataBaseFactory();
            uow = new UnitOfWork(dbf);
            claimS = new Service<Claims>(uow);
            produictS = new Service<Product>(uow);
        }
        // GET: Claims
        public ActionResult Index()
        {
            return View(claimS.GetAll());
        }

        // GET: Claims/Details/5
        public ActionResult Details(int id)
        {
            return View(claimS.GetById(id));
        }

        // GET: Claims/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Claims/Create
        [HttpPost]
        public ActionResult Create(Claims e)
        {
            claimS.Add(e);
            claimS.Commit();
            return RedirectToAction("Index");
        }

        // GET: Claims/Edit/5
        public ActionResult Edit(int id)
        {
            return View(claimS.GetById(id));
        }

        // POST: Claims/Edit/5
        [HttpPost]
        public ActionResult Edit(Claims d)
        {
            try
            {
                // TODO: Add delete logic here
                claimS.Update(d);
                claimS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Claims/Delete/5
        public ActionResult Delete(int id)
        {
            return View(claimS.GetById(id));
        }

        // POST: Claims/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                claimS.Delete(claimS.GetById(id));
                claimS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
