﻿using ConsomiTounsi.Data;
using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{
    public class DeliveryController : Controller
    {

        IDataBaseFactory dbf;
        IUnitOfWork uow;
        IService<Delivery> deliveryS;
        IService<DeliveryMan> deliveryManS;

        public DeliveryController()
        {
            dbf = new DataBaseFactory();
            uow = new UnitOfWork(dbf);
            deliveryS = new Service<Delivery>(uow);
            deliveryManS=new Service<DeliveryMan>(uow);
        }
        // GET: Delivery
        public ActionResult Index()
        {
            return View(deliveryS.GetAll());
        }

        public ActionResult Validation()
        {
            return View();
        }

        // GET: Delivery/Details/5
        public ActionResult Details(int id)
        {
            return View(deliveryS.GetById(id));
        }

        // GET: Delivery/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Delivery/Create
        [HttpPost]
        public ActionResult Create(Delivery d)
        {
            deliveryS.Add(d);
            deliveryS.Commit();
            return RedirectToAction("Index");
        }

        // GET: Delivery/Edit/5
        public ActionResult Edit(int id)
        {
            return View(deliveryS.GetById(id));
        }

        // POST: Delivery/Edit/5
        [HttpPost]
        public ActionResult Edit(Delivery e)
        {
            try
            {
                // TODO: Add delete logic here
                deliveryS.Update(e);
                deliveryS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Delivery/Delete/5
        public ActionResult Delete(int id)
        {
            return View(deliveryS.GetById(id));
        }

        // POST: Delivery/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                deliveryS.Delete(deliveryS.GetById(id));
                deliveryS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Confirmer(int id,int Man)
        {
            ConsomiCtxt ctx = new ConsomiCtxt();
            var change= deliveryS.GetById(id);
            var changeM = deliveryManS.GetById(Man);
            if ( changeM.MyDelivery.DeliveryState!="yes")
            {

                change.DeliveryState = "yes";
                changeM.DeliveryId = null;

                int som = int.Parse(changeM.ImageCin_d) + 1;
                changeM.ImageCin_d =som.ToString();  
                
                
                deliveryS.Commit();
                return RedirectToAction("Index", "DeliveryMan");

            }
            else {

                changeM.DeliveryId = null;
                deliveryS.Commit();

                return RedirectToAction("Validation"); }
       

        
    }

        // POST: Delivery/confirmer/5
        [HttpPost]
        public ActionResult Confirmer()
        {
           
            return View();


        }


        //public JsonResult ListDelivery()
        //{
        //    var maplist = deliveryS.GetAll();

        //    return Json(maplist, JsonRequestBehavior.AllowGet);
        //}



    }
}
