﻿using ConsomiTounsi.Data;
using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using Postal;
using System.Web.Helpers;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{
    public class DeliveryManController : Controller
    {

        IDataBaseFactory dbf;
        IUnitOfWork uow;
        IService<DeliveryMan> deliveryManS;
        IService<Delivery> serviceD;

        public DeliveryManController()
        {
            dbf = new DataBaseFactory();
            uow = new UnitOfWork(dbf);
            deliveryManS = new Service<DeliveryMan>(uow);
            serviceD = new Service<Delivery>(uow);
        }
        // GET: DeliveryMan
        public ActionResult Index()
        {
            return View(deliveryManS.GetAll());
        }

        public JsonResult ListMap()
        {
           var maplist = deliveryManS.GetAll();
           
            return Json(maplist,JsonRequestBehavior.AllowGet);
        }


        // GET: DeliveryMan/Details/5
        public ActionResult Details(int id)
        {
            return View(deliveryManS.GetById(id));
        }

        // GET: DeliveryMan/Create
        public ActionResult Create()
        {
            ViewBag.DeliveryId = new SelectList(serviceD.GetAll(), "DeliveryId", "DeliveryId");
            return View();
        }

        // POST: DeliveryMan/Create
        [HttpPost]
        public ActionResult Create(DeliveryMan dm)
        {
            deliveryManS.Add(dm);
            deliveryManS.Commit();
            return RedirectToAction("Index");
        }

        // GET: DeliveryMan/Edit/5
        public ActionResult Edit(int id)
        {

            DeliveryMan prod = deliveryManS.GetById(id);
            ViewBag.DeliveryId = new SelectList(serviceD.GetAll(), "DeliveryId", "Ville",prod.DeliveryId);
            //ViewBag.CategoryId = new SelectList(serviceCat.GetAll(), "CategoryId", "Name", prod.CategoryId);
            return View(prod);
        }

        // POST: DeliveryMan/Edit/5
        [HttpPost]
        public ActionResult Edit(DeliveryMan e)
        {

            //var info = deliveryManS.GetById(id);
            dynamic email = new Email("Example");
            email.To = e.UsersEmail_d;
            email.Message = "Connectez-vous pour confirmer";
           email.Send();

            
                deliveryManS.Update(e);
                deliveryManS.Commit();
                

                return RedirectToAction("Index");
          
        }

        // GET: DeliveryMan/Delete/5
        public ActionResult Delete(int id)
        {
            return View(deliveryManS.GetById(id));
        }

        // POST: DeliveryMan/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                deliveryManS.Delete(deliveryManS.GetById(id));
                deliveryManS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult ConfirmerMan(int id)
        {
            ConsomiCtxt ctx = new ConsomiCtxt();
            var change = deliveryManS.GetById(id);
            change.DeliveryId=null;
           

            deliveryManS.Commit();
            return RedirectToAction("Index");
        }


        public ActionResult AffecterLivraison(int id)
        {
            ViewBag.DeliveryId = new SelectList(serviceD.GetAll(), "DeliveryId", "DeliveryId");
            
            return View(deliveryManS.GetById(id));
        }

        // POST: DeliveryMan/Edit/5
        [HttpPost]
        public ActionResult AffecterLivraison(DeliveryMan e)
        {
            try
            {
                // TODO: Add delete logic here
                deliveryManS.Update(e);
                deliveryManS.Commit();
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        public ActionResult Chart()
        {

            
            var change = deliveryManS.GetAll();
           

            foreach (var p in change)
            { 


                var a = int.Parse(p.ImageCin_d);
                var b = p.UsersName_d;

                new Chart(width: 800, height: 280)


               .AddSeries(
                          chartType: "column",
                          xValue: new[] { b },
                          yValues: new[] { a }).Write("png");

            }

                return null ;
            
           
        }

    }
}
