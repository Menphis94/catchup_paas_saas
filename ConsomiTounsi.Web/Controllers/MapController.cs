﻿using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ConsomiTounsi.Web.Controllers
{
    public class MapController : Controller
    {

        IDataBaseFactory dbf;
        IUnitOfWork uow;
        IService<DeliveryMan> deliveryManS;
        IService<Delivery> serviceD;

        public MapController()
        {
            dbf = new DataBaseFactory();
            uow = new UnitOfWork(dbf);
            deliveryManS = new Service<DeliveryMan>(uow);
            serviceD = new Service<Delivery>(uow);
        }
        // GET: Map
        public ActionResult Index()
        { 
            
            return View();
        }
       
    }
}