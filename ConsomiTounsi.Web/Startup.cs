﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ConsomiTounsi.Web.Startup))]
namespace ConsomiTounsi.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
