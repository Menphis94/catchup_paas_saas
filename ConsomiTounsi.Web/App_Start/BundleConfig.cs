﻿using System.Web;
using System.Web.Optimization;

namespace ConsomiTounsi.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));

            #region Template design

            bundles.Add(new ScriptBundle("~/template/js").Include(
                       "~/Scripts/plugins/bower_components/jquery/dist/jquery.min.js",
                        "~/Scripts/bootstrap/dist/js/tether.min.js",
                         "~/Scripts/plugins/bower_components/bootstrap-extension/js/bootstrap-extension.min.js",
                          "~/Scripts/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js",
                           "~/Scripts/js/jquery.slimscroll.js",
                            "~/Scripts/js/waves.js",
                            "~/Scripts/js/dashboard1.js",
                            "~/Scripts/plugins/bower_components/jquery/dist/jquery.js",
                             "~/Scripts/plugins/bower_components/waypoints/lib/jquery.waypoints.js",
                              "~/Scripts/plugins/bower_components/counterup/jquery.counterup.min.js",
                               "~/Scripts/plugins/bower_components/raphael/raphael-min.js",
                                
                               "~/Scripts/js/custom.min.js",
                                "~/Scripts/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js",
                                 "~/Scripts/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js",
                                  "~/Scripts/js/dashboard1.js",
                                   "~/Scripts/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js",
                                    "~/Scripts/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js",
                                     "~/Scripts/plugins/bower_components/styleswitcher/jQuery.style.switcher.js"



                       ));

            bundles.Add(new StyleBundle("~/template/css").Include(
                     "~/Content/bootstrap/dist/css/bootstrap.min.css",
                     "~/Content/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.css",
                     "~/Content/plugins/bower_components/toast-master/css/jquery.toast.css",
                     "~/Content/plugins/bower_components/morrisjs/morris.css",
                     "~/Content/css/animate.css",
                     "~/Content/css/style.css",
                     "~/Content/css/colors/default.css",
                 
                     "~/Content/plugins/bower_components/bootstrap-extension/css/bootstrap-extension.css"));




            bundles.Add(new StyleBundle("~/front/css").Include(
                     "~/Content/css/bootstrap.min.css",
                      "~/Content/css/slick.css",
                       "~/Content/css/slick-theme.css",
                        "~/Content/css/nouislider.min.css",
                         "~/Content/css/font-awesome.min.css",
                         "~/Content/css/style1.css"
                     ));




            bundles.Add(new ScriptBundle("~/front/js").Include(
                      "~/Scripts/js/jquery.min.js",
                      "~/Scripts/js/bootstrap.min.js",
                      "~/Scripts/js/slick.min.js",
                      "~/Scripts/js/nouislider.min.js",
                      "~/Scripts/js/jquery.zoom.min.js",
                      "~/Scripts/js/main.js"



                          ));


            #endregion
        }
    }
}
