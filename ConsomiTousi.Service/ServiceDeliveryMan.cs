﻿using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsomiTounsi.Data.Infrastructure;

namespace ConsomiTousi.Service
{
    public class ServiceDeliveryMan : Service<DeliveryMan>, IServiceDeliveryMan
    {
        public ServiceDeliveryMan(IUnitOfWork _uow) : base(_uow)
        {
        }


        public IEnumerable<DeliveryMan> GetByDelivery(int deliveryId)
        {
            return GetMany(p => p.DeliveryId == deliveryId);
        }
    }
}
