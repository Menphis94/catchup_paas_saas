﻿using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsomiTounsi.Data.Infrastructure;

namespace ConsomiTousi.Service
{
    public class ServiceDelivery : Service<Delivery>, IServiceDelivery
    {
        public ServiceDelivery(IUnitOfWork _uow) : base(_uow)
        {
        }

        public IEnumerable<Delivery> GetByDelivery(int deliveryId)
        {
            return GetMany(p => p.DeliveryId == deliveryId);
        }
    }
}
