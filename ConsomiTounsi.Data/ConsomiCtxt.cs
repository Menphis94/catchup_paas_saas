﻿using ConsomiTousi.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTounsi.Data
{
    public class ConsomiCtxt : DbContext
    {


        public DbSet<Product> Products { get; set; }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Claims> Claimes { get; set; }
        public DbSet<Basket> Baskets { get; set; }
        public DbSet<Users> User { get; set; }
        public DbSet<Factures> Facturs { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Delivery> Deliveries { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Publish> Publishs { get; set; }
        public DbSet<PublishComent> PublishComents { get; set; }
        public DbSet<DeliveryMan> DeliveryMans { get; set; }
        public ConsomiCtxt() : base("name=MaChaine")
        {

        }
    }
}
