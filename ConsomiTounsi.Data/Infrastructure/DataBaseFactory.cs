﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTounsi.Data.Infrastructure
{
    public class DataBaseFactory : Disposable, IDataBaseFactory
    {
        ConsomiCtxt ctxt;
        public DataBaseFactory()
        {
            ctxt = new ConsomiCtxt();
        }
        public ConsomiCtxt Ctxt { get { return ctxt; } }
        public override void DisposeCore()
        {
            if (ctxt != null)
                ctxt.Dispose();
        }
    }
}
