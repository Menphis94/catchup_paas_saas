﻿using ConsomiTounsi.Data;
using ConsomiTounsi.Data.Infrastructure;
using ConsomiTousi.Domain;
using ConsomiTousi.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsomiTounsi.Console
{
   public class Program
    {
        static void Main(string[] args)
        {

            Category cat1 = new Category() { Name = "CAT1" };
            
            ConsomiCtxt cxt = new ConsomiCtxt();
            cxt.Categories.Add(cat1);
            cxt.SaveChanges();

        }
    }
}
